import React from 'react';
import { firebaseApp } from "./firebase.js"
import "./App.css"
import "./css/tailwind.css"

import Presentation from "./Components/Presentation/Presentation";
//import SkillsContainer from "./Components/Skills/SkillsScontainer";
//import ProjectsContainer from "./Components/Projects/ProjectContainer";
//import Contact from "./Components/Contact/Index";
import Start from "./Components/StartPage/Index"

import { BrowserRouter as Router, Switch, Route } from "react-router-dom";


export default class App extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      user: {
        name: "",
        ownImg: "",
        mySkills: [],
        description: [],
        myProjects: [],
        heartLikes: 0
      }
    }

    this.onSwitchHeart=this.onSwitchHeart.bind(this)
    this.dbRef = firebaseApp.database().ref().child("user")
  }


  componentWillMount() {
    this.fetchUserData(this.dbRef)
  }


  onSwitchHeart(){
    this.dbRef.update({ heartLikes: this.state.heartLikes+1 })    
  }

  fetchUserData(ref) {
    ref.on("value", snap => {
      const loadUsr = snap.val();
      this.setState({ user: loadUsr })
    })
  }



  render() {
    const { ownImg, name, heartLikes, description, mySkills, myProjects } = this.state.user;
   
    return (
      <Router>


        <Switch>

          <Route exact path="/">
            <Start />
          </Route>

          <Route path="/presentation">
            <div className="flex flex-col p-4 text-gray-800 tipografiaPrincipal">
              <Presentation
                img={ownImg}
                name={name}
                heartLikes={heartLikes}
                description={description}
                skills={mySkills}
                projects={myProjects}
                switchHeart={this.onSwitchHeart.bind(this)} />
            </div>
          </Route>

        </Switch>


      </Router>
    )
  }
}
