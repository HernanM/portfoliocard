import React, { Component } from 'react';
import "./style.css";
import Skills from "../Skills/SkillsScontainer"
import Projects from "../Projects/ProjectContainer"
import CorazonVacio from "./Cvacio.png"
import CorazonLleno from "./Clleno.png"

export default class Presentation extends Component {
    constructor(props) {
        super(props)

        this.state = {

            likesNumber: this.props.heartLikes,
            heart: CorazonVacio
        }
        console.log("this.props.heartLikes:::",this.props.heartLikes)
        //this.setState({likesNumber:this.props.likesNumber})

        
    }



    render() {
        const { img, name, description, skills, projects ,switchHeart} = this.props
        const { heart } = this.state
        
        return (
            <div className="mt-1">

                <div style={{ backgroundColor: "#f2f2f2" }} className="flex flex-col rounded-lg overflow-hidden shadow-lg max-w-sm">
                    <div className="relative">
                        <img style={{ height: "340px" }} className="" src={img} alt="" />
                        <div className="absolute bottom-0 right-0 mb-3 mr-3 ">
                            <img onClick={()=>switchHeart()} className="h-10 w-10" src={heart} alt="" />
                        </div>
                    </div>
                    <div className="px-6 my-4">
                        <h2 className="mb-4 text-3xl font-bold leading-none tracking-wide" >{name}</h2>

                        <div>
                            <div className="mb-1 flex flex-row">
                                <h3 className="text-lg font-light">Product Description</h3>
                            </div>

                            {description.map(des => <p className="text-gray-600 leading-tight "> {des}</p>)}

                            <div style={{ borderborderColor: "#e0e0e0" }} className="my-3 border-b"></div>
                        </div>

                        <div>
                            <div className="mb-1 flex flex-row mb-1">
                                <h3 className="text-lg font-light">Usefull for</h3>
                            </div>

                            <Skills skills={skills} />

                            <div style={{ borderColor: "#e0e0e0" }} className="my-3 border-b"></div>
                        </div>

                        <div>
                            <div className="mb-1 flex flex-row mb-1">
                                <h3 className="text-lg font-light">Other Products</h3>
                            </div>

                            <Projects projects={projects} />

                            <div style={{ borderColor: "#e0e0e0" }} className="my-3 border-b"></div>
                        </div>

                    </div>

                </div>

            </div >
        );
    }
}
