import React, { Component } from "react";
import "./estilos.css"


export default class Project extends Component {

    render() {

        const { name, imgUrl, repoUrl } = this.props.info;


        return (
            <div className="background-container mb-2 w-full ">

                <img className="" src={imgUrl} alt="" />

                <a href={repoUrl}> <h1 className="project-name uppercase text-lg font-medium tracking-wide"> {name} </h1></a>

            </div>
        );
    }
}