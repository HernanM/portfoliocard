import React, { Component } from "react";
import Project from "./Project";



export default class ProjectContainer extends Component {
    render() {
        const { projects } = this.props;

        return (
            <div>
                <div className="flex flex-col text-center items-center">
                    {projects.map(project => <Project info={project} key={project.name} />)}
                </div>
            </div>
        );
    }
}