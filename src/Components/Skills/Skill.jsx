import React, { Component } from "react";
import questImg from "./question.png"
export default class Skill extends Component {


    getLevelStyle(levelNumber) {
        return (levelNumber === 1 ? { porcentage: "25%", color: "bg-red-300" } : levelNumber === 2 ? { porcentage: "50%", color: "bg-yellow-300" } : levelNumber === 3 ? { porcentage: "75%", color: "bg-blue-300" } : { porcentage: "100%", color: "bg-green-300" });
    }

    render() {
        const { name, level, page } = this.props.info;
        const levelStyle = this.getLevelStyle(level);

        return (

            <div className="w-full leading-tight text-gray-600 mb-2">

                <div className="flex flex-row justify-between">
                    <div className="flex">
                        <a href={page}>
                            <img style={{ marginTop: "3px" }} className="w-3 h-3 mr-1" src={questImg} alt="" />
                        </a>
                        <h1 className=" ">{name}</h1>
                    </div>
                    <h1 className={` `} >{levelStyle.porcentage}</h1>
                </div>
                <div className={`${levelStyle.color} uppercase font-bold leading-tight h-1 `} style={{ width: levelStyle.porcentage }}></div>

                {/* <div className="shadow w-full bg-gray-100 ">
                    <div className={`${levelStyle.color} uppercase font-bold leading-tight py-1 text-white h-6 `} style={{ width: levelStyle.porcentage }}><pre> {name} </pre></div>
                </div> */}

            </div>

        );
    }
}