import React, { Component } from "react"
import cartImg from "./cart.png"
import magglas from "./lupa.png"
import { Link } from "react-router-dom"

export default class Index extends Component {



    render() {

        return (
            <div className="relative h-height">
                <div style={{ backgroundColor: "#EDEEF0" }} className="flex flex-row justify-between items-center h-12 shadow w-full ">

                    <div className="flex flex-row items-center">
                        <h1>hola</h1>
                        <div className="flex items-center bg-white ml-4 py-1 rounded">
                            <img className="h-4 w-4 ml-2" src={magglas} alt="" />
                            <p className="ml-3 pr-8 text-gray-500">Search for products...</p>
                        </div>
                    </div>

                    <Link to="/presentation">
                        <div className="relative">
                            <div className="absolute w-6 bg-green-500 rounded-full bottom-0 right-0 mr-3 text-center text-white font-bold">1</div>
                            <img className="mr-5 h-10 w-10" src={cartImg} alt="" />
                        </div>
                    </Link>

                </div>

                <div className="absolute mt-3 mr-5 right-0">
                    <div className="p-2 bg-red-600 rounded-full text-center w-40 animated infinite heartBeat delay-1s">
                        <h1 className="text-white font-bold text-lg tracking-wide">Press the cart!</h1>
                    </div>
                </div>

                <div className="flex flex-row">
                    <div className="w-2/3 h-12 bg-red-400">

                    </div>
                    <div className="w-1/3 h-12 bg-blue-400">

                    </div>
                </div>

            </div>)
    }

}