import * as firebase from "firebase";

const firebaseConfig = {
    apiKey: "AIzaSyCZzg8x2j8FJ9JFJd1HjUUp1e-0Y-FP698",
    authDomain: "miportfolio-eb786.firebaseapp.com",
    databaseURL: "https://miportfolio-eb786.firebaseio.com",
    projectId: "miportfolio-eb786",
    storageBucket: "miportfolio-eb786.appspot.com",
    messagingSenderId: "589519971548",
    appId: "1:589519971548:web:acf68061bf3e99da770019"
  };

  firebase.initializeApp(firebaseConfig);
  
  const firebaseApp = firebase;
  
  export{
      firebaseApp
  };